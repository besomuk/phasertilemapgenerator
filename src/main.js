import Phaser from 'phaser'

import HelloWorldScene from './scenes/HelloWorldScene'
import MyScene from './scenes/MyScene'
import BoardScene from './scenes/BoardScene'

const config = {
	type: Phaser.AUTO,
	width: 800,
	height: 600,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: { y: 0 }
		}
	},
	scene: [BoardScene, MyScene, HelloWorldScene]
}

export default new Phaser.Game(config)
