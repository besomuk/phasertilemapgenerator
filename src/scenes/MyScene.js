import Phaser from 'phaser'

export default class MyScene extends Phaser.Scene
{
	constructor()
	{
		super('hello-my-world');
	}

	preload()
    {
        console.log ("constructor");
        this.load.setBaseURL('http://labs.phaser.io')

        //this.load.image('sky', 'assets/skies/space3.png')
        //this.load.image('logo', 'assets/sprites/phaser3-logo.png')
        this.load.image('red', 'assets/particles/red.png');
        this.load.image('blue', 'assets/particles/blue.png');
    }

    create()
    {
        console.log ("create...");
        //this.setParticles();
        this.drawSomething();
        /*
        this.add.image(400, 300, 'sky')

        const particles = this.add.particles('red')

        const emitter = particles.createEmitter({
            speed: 100,
            scale: { start: 1, end: 0 },
            blendMode: 'ADD'
        })

        const logo = this.physics.add.image(400, 100, 'logo')

        logo.setVelocity(100, 200)
        logo.setBounce(1, 1)
        logo.setCollideWorldBounds(true)

        emitter.startFollow(logo)*/
    }
    
    drawSomething()
    {
        /*
        var path;
        path = new Phaser.Curves.Path(1500, 500);

        path.lineTo(700, 500);
        path.splineTo([ 745, 256, 550, 145, 300, 250, 260, 450, 50, 500 ]);
        path.lineTo(-100, 500);
        */
        
        var path2 = new Phaser.Curves.Path(400, 300).circleTo(150).moveTo(400, 300).circleTo(150, true, 180);
        
        var graphics = this.add.graphics();
        graphics.lineStyle(1, 0xffffff, 1);
        path2.draw(graphics, 128);
        
        const particles = this.add.particles('blue');
        
        const emitter = particles.createEmitter
            ({
                speed: 2,
                scale: { start: 1, end: 0 },
                blendMode: 'ADD',
                emitZone: { type: 'edge', source: path2, quantity: 64, yoyo: false }
            });

        const particles2 = this.add.particles('red');
        
        const emitter2 = particles2.createEmitter
            ({
                x: 50,
                speed: 2,
                scale: { start: 1, end: 0 },
                blendMode: 'ADD',
                emitZone: { type: 'edge', source: path2, quantity: 64, yoyo: false }
            });
    
        /*
        particles.createEmitter({
            frame: { frames: [ 'red', 'green', 'blue' ], cycle: true },
            scale: { start: 0.5, end: 0 },
            blendMode: 'ADD',
            emitZone: { type: 'edge', source: path2, quantity: 48, yoyo: false }
        });*/
        
    }
    
    setParticles()
    {
        console.log ("Set particles...");
        console.log ( this.sys.game.scale.gameSize._width );
        
        const particles = this.add.particles('red');

        const emitter = particles.createEmitter
            ({
                x: this.sys.game.scale.gameSize._width / 2,
                y: this.sys.game.scale.gameSize._height / 2,
                speed: 200,
                scale: { start: 1, end: 0 },
                blendMode: 'ADD'
            })
        
        /*
        const logo = this.physics.add.image(400, 100, 'logo')

        logo.setVelocity(100, 200)
        logo.setBounce(1, 1)
        logo.setCollideWorldBounds(true)

        emitter.startFollow(logo)*/        
    }
}