/* Creates tile */
import Phaser from 'phaser'

let nnn = 'asd'

const tileWidth = 64;

export default class TileCreator
{
    /* 
        tileKey           - key for getting Phaser resources
        name              - name of tile
        gridPositionX & Y - position of tile in 2 dim array
        scale             - tile scale, used for scaling tiles, that is entire map
    */
    constructor ( scene, tileKey = 'tile', name = 'name', gridPositionX = 0, gridPositionY = 0, scale = 1 )
    {
        this.scene = scene;
        this.key   = tileKey;
        this.scale  = scale;
        this.name = name;
        this.gridPosition = 
            {
                x: gridPositionX,
                y: gridPositionY  
            };
        nnn = this.name;
        
        this._group = this.scene.physics.add.group();
        
    }
    
    get group ()
    {
        return this._group;
    }
    
    tileWidth ()
    {
        return tileWidth;
    }
    
    tileScale ()
    {
        return this.scale;
    }
    
	spawn ( x = 0, y = 0 )
	{
		//const x = (playerX < 400) ? Phaser.Math.Between(400, 800) : Phaser.Math.Between(0, 400);
        const tile = this.group.create(x, y, this.key);
        tile.setScale(this.scale);
        tile.setBounce(1);
        tile.setCollideWorldBounds(true);
		//tile.setVelocity(Phaser.Math.Between(-200, 200), 20);
        tile.setInteractive();        
        tile.on('pointerdown', name => this.doClick() );
		
		return tile;
	} 
    
    getName ()
    {
        return this.name;
    }
    
    doClick()
    {
        console.log ("name: " + this.getName() );
        console.log ("grid: ", this.gridPosition );
    }
}