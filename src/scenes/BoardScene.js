import Phaser from 'phaser'

import TileCreator from './TileCreator'

const TILE_KEY_GRASS   = 'tile_grass';
const TILE_KEY_TREE_A_1  = 'tile_tile_tree01';
const TILE_KEY_TREE_A_2  = 'tile_tile_tree02';
const TILE_KEY_TREE_A_3  = 'tile_tile_tree03';
const TILE_KEY_TREE_A_4  = 'tile_tile_tree04';
const TILE_KEY_TREE_B_1  = 'tile_tile_tree05';
const TILE_KEY_TREE_B_2  = 'tile_tile_tree06';
const TILE_KEY_TREE_B_3  = 'tile_tile_tree07';
const TILE_KEY_TREE_B_4  = 'tile_tile_tree08';
const TILE_KEY_STUMP_1   = 'tile_tile_tree09';
const TILE_KEY_STUMP_2   = 'tile_tile_tree10';
const TILE_KEY_STUMP_3   = 'tile_tile_tree11';
const TILE_KEY_STUMP_4   = 'tile_tile_tree12';

export default class BoardScene extends Phaser.Scene
{
	constructor()
	{
		super('board-scene')
        
        this.tileSpawn = undefined; 
	}

	preload()
    {
        console.log ("Preload Board Scene...");
        //this.load.setBaseURL('http://labs.phaser.io');

        //this.load.image('sky', 'assets/skies/space3.png');
        this.load.image(TILE_KEY_GRASS, '/assets/tiles/med/Tile/medievalTile_58.png');
        this.load.image(TILE_KEY_TREE_A_1, '/assets/tiles/med/Tile/medievalTile_41.png');
        this.load.image(TILE_KEY_TREE_A_2, '/assets/tiles/med/Tile/medievalTile_42.png');
        this.load.image(TILE_KEY_TREE_A_3, '/assets/tiles/med/Tile/medievalTile_43.png');
        this.load.image(TILE_KEY_TREE_A_4, '/assets/tiles/med/Tile/medievalTile_44.png');
        this.load.image(TILE_KEY_TREE_B_1, '/assets/tiles/med/Tile/medievalTile_45.png');
        this.load.image(TILE_KEY_TREE_B_2, '/assets/tiles/med/Tile/medievalTile_46.png');
        this.load.image(TILE_KEY_TREE_B_3, '/assets/tiles/med/Tile/medievalTile_47.png');
        this.load.image(TILE_KEY_TREE_B_4, '/assets/tiles/med/Tile/medievalTile_48.png');
        this.load.image(TILE_KEY_STUMP_1, '/assets/tiles/med/Tile/medievalTile_49.png');
        this.load.image(TILE_KEY_STUMP_2, '/assets/tiles/med/Tile/medievalTile_50.png');
        this.load.image(TILE_KEY_STUMP_3, '/assets/tiles/med/Tile/medievalTile_51.png');
        this.load.image(TILE_KEY_STUMP_4, '/assets/tiles/med/Tile/medievalTile_52.png');
        //this.load.image('logo', 'assets/sprites/phaser3-logo.png')
        //this.load.image('red', 'assets/particles/red.png')
    }

    create()
    {
        console.log ("Create Board Scene...");
        
        let mapData = this.generateMapValues( 15, 11 ); // generate map
        this.drawTileMap( mapData, 50, 50, 0.75);    // draw map
    }
    
    /* Map is collection of values within two dimensional array. Each value represents one tile type
       1 - grass, 2 - trees, etc. width and height are array lengths.
       */
    generateMapValues ( mapWidth = 10, mapHeight = 8 )
    {
        // map properties..
        let treePercentage = 40;
        
        // Dynamic array declaration
        let mapArray = new Array(mapHeight);
        for (var i = 0; i < mapArray.length; i++) 
        {
            mapArray[i] = new Array(mapWidth);
        }        
        
        for ( var i = 0; i < mapHeight; i++ )
        {
            for ( var j = 0; j < mapWidth; j++ )
            {
                var rndNumber = Math.floor(Math.random() * 100) + 1; // give number 1 to 100, probability for grass tile
                var tileNumber = 0;
                if ( rndNumber < ( 100 - treePercentage ) ) // if lesser than rndNumber, grow grass
                    tileNumber = 1;
                else
                    tileNumber = 2;
                mapArray[i][j] = tileNumber;
            }                
        }
        // End of dynamic array declaration
        
        console.log ( mapArray );
        
        // make map data. I want map and it's width and height returned.
        let mapData =
            {
                map: mapArray,
                mapWidth: mapWidth,
                mapHeigth: mapHeight
            }
        
        return mapData;
    }
    
    /* Draws map based on mapData. 
        startX,Y - where map starts on screen
        mapScale - scale of one tile, therefore entire map
        */
    drawTileMap ( mapData, startX = 50, startY = 50, mapScale = 1)
    {
        let map = mapData.map;
        console.log ( map );
        // Draw map, based on array        
        let counter = 0;
        let gap = 0; // gap between tiles
        
        let newX = startX;
        let newY = startY;
        
        for ( var i = 0; i < mapData.mapHeigth; i ++ )
        {
            for ( var j = 0; j < mapData.mapWidth; j ++ )
            {
                var tempKey = "";
                var tempName = "";
                if ( map[i][j] == 1 )
                {
                    tempKey = TILE_KEY_GRASS;
                    tempName = "grassTile";
                }
                if ( map[i][j] == 2 )
                {
                    
                    tempKey = this.chooseTree(false, true);          
                    tempName = "treeTile";
                }
                
                let newName = tempName + " " + j + " " + i;
                this.tileSpawn = new TileCreator(this, tempKey, newName, j, i, mapScale );
                this.tileSpawn.spawn ( newX, newY );
                newX += this.tileSpawn.tileWidth() * this.tileSpawn.tileScale() + gap;
            }
            newX = startX; 
            newY += this.tileSpawn.tileWidth() * this.tileSpawn.tileScale() + gap;
        }
    }
    
    /* Chooses tree tile.
        If evergreen, evergreen tiles will be used.
        If stumps, there will be stumps on the field. Stump percent is determined by stumpsPercent
        */
    chooseTree ( evergreen = false, stumps = false, stumpsPercent = 2 )
    {
        if ( stumps )
        {
            var rndNumber = Math.floor(Math.random() * 100) + 1;
            
            if ( rndNumber <= stumpsPercent )
            {
                var rndNumber = Math.floor(Math.random() * 4) + 1;
                if ( rndNumber == 1 ) return TILE_KEY_STUMP_1;
                if ( rndNumber == 2 ) return TILE_KEY_STUMP_2;
                if ( rndNumber == 3 ) return TILE_KEY_STUMP_3;
                if ( rndNumber == 4 ) return TILE_KEY_STUMP_4;                
            }
        }

        if ( evergreen )
        {
            var rndNumber = Math.floor(Math.random() * 4) + 1;
            if ( rndNumber == 1 ) return TILE_KEY_TREE_B_1;
            if ( rndNumber == 2 ) return TILE_KEY_TREE_B_2;
            if ( rndNumber == 3 ) return TILE_KEY_TREE_B_3;
            if ( rndNumber == 4 ) return TILE_KEY_TREE_B_4;            
        }
        else
        {
            var rndNumber = Math.floor(Math.random() * 4) + 1;
            if ( rndNumber == 1 ) return TILE_KEY_TREE_A_1;
            if ( rndNumber == 2 ) return TILE_KEY_TREE_A_2;
            if ( rndNumber == 3 ) return TILE_KEY_TREE_A_3;
            if ( rndNumber == 4 ) return TILE_KEY_TREE_A_4;
        }
    }
    
    createRandomTiles ( startX = 50, startY = 50, count = 3 )
    {
        let counter = 0;
        let gr1 = 64;
        let gr2 = 5;
        for ( var i = 0; i < count; i++)
        {
            let newName = 'ime' + counter;
            this.tileSpawn = new TileCreator(this, TILE_KEY_TREE_1, newName );
            this.tileSpawn.spawn ( startX, startY );
            startX += this.tileSpawn.tileWidth() * this.tileSpawn.tileScale() + 5;
            
            counter ++;
            
            console.log ( this.tileSpawn.getName() );
        }
    }
    
}
